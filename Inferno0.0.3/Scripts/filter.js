pc.extend(pc, function () {
    var SpeedEffect = function (graphicsDevice) {
        // User Accessible Uniforms:
        this.streak_length = 3.0
        this.zoom_amt = 1.0
        this.fisheye_amt = 2.0
        this.chromatic_abbr = 0.00
        this.streak_center_y = 0.5
        this.streak_center_x = 0.5

        //Private uniform:
        this._streak_offset = 0.0

        //Shaders metagumphf:
        this.needsDepthBuffer = true;
        var attributes = {
            aPosition: pc.SEMANTIC_POSITION
        };

        var passThroughVert = ["attribute vec2 aPosition;",
"varying vec2 vUv0;",
"void main(void)",
"{",
"    gl_Position = vec4(aPosition, 0.0, 1.0);",
"    vUv0 = (aPosition.xy + 1.0) * 0.5;",
"}",
""].join("\n")

        var luminosityFrag = "precision " + graphicsDevice.precision + " float;\n" +
            ["uniform sampler2D uColorBuffer;",
"uniform sampler2D uDepthBuffer;",
"",
"#define STREAKS 1000.0",
"",
"uniform float streak_length;",
"uniform float zoom_amt;",
"uniform float chromatic_abbr;",
"uniform float fisheye_amt;",
"uniform float streak_center_y;",
"uniform float streak_center_x;",
"varying vec2 vUv0;",
"",
"uniform float streak_offset;",
"",
"float rand(float num){",
"    return 0.5 + 0.5 * fract(sin(num * 78.233) * 437585.5453);",
"}",
"",
"float getangle(vec2 coords){",
"    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);",
"    return float(int(atan(normalized.x / normalized.y) * STREAKS));",
"}",
"",
"vec2 noisyzoom(vec2 coords, float zoom, float depth){",
"    vec2 normalized = coords - vec2(streak_center_x, streak_center_y);",
"    float dist = length(normalized);",
"",
"    // Streaks:",
"    float streaks = rand(getangle(coords)+streak_offset);",
"    streaks *= pow(dist, 2.0);",
"    streaks *= streak_length;",
"    streaks *= depth;",
"    normalized *= 1.0 - streaks;",
"",
"    // Zoom:",
"    normalized *= zoom;",
"",
"    return normalized + vec2(streak_center_x, streak_center_y);",
"}",
"",
"",
"vec2 fisheye(vec2 coords){",
"    vec2 normalized = coords - vec2(0.5, 0.5);",
"    float dist = length(normalized);",
"    return normalized * (dist + fisheye_amt) / (fisheye_amt + 1.0) + vec2(0.5, 0.5);",
"}",
"",
"float unpackFloat(vec4 rgbaDepth){",
"    const vec4 bitShift = vec4(1.0 / (256.0 * 256.0 * 256.0), 1.0 / (256.0 * 256.0), 1.0 / 256.0, 1.0);",
"    return dot(rgbaDepth, bitShift);",
"}",
"",
"void main(void){",
"    vec2 orig_coord = vUv0;",
"",
"    orig_coord = fisheye(orig_coord);",
"",
"    //float depth = clamp(1.0 - pow(texture2D(uDepthBuffer, orig_coord).x, 4.0), 0.0, 1.0);",
"    float depth = pow(unpackFloat(texture2D(uDepthBuffer, orig_coord)), 0.2);",
"    float close = clamp((1.0 - depth*2.2), 0.0, 1.0);",
"",
"    vec2 texcoord = noisyzoom(orig_coord, zoom_amt, close);",
"    gl_FragColor.r = texture2D(uColorBuffer, texcoord).r;// + (depth);",
"",
"    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr, close);",
"    gl_FragColor.g = texture2D(uColorBuffer, texcoord).g;",
"",
"    texcoord = noisyzoom(orig_coord, zoom_amt + chromatic_abbr * 2.0, close);",
"    gl_FragColor.b = texture2D(uColorBuffer, texcoord).b;",
"",
"    gl_FragColor.a = 1.0;",
"}",
""].join("\n")

        this.speedShader = new pc.Shader(graphicsDevice, {
            attributes: attributes,
            vshader: passThroughVert,
            fshader: luminosityFrag
        });
    };

    SpeedEffect = pc.inherits(SpeedEffect, pc.PostEffect);

    SpeedEffect.prototype = pc.extend(SpeedEffect, {
        render: function (inputTarget, outputTarget, rect) {
            var device = this.device;
            var scope = device.scope;

            scope.resolve("uColorBuffer").setValue(inputTarget.colorBuffer);
            scope.resolve("uDepthBuffer").setValue(this.depthMap);
            scope.resolve("streak_length").setValue(this.streak_length);
            scope.resolve("zoom_amt").setValue(this.zoom_amt);
            scope.resolve("fisheye_amt").setValue(this.fisheye_amt);
            scope.resolve("chromatic_abbr").setValue(this.chromatic_abbr);
            scope.resolve("streak_center_y").setValue(this.streak_center_y);
            scope.resolve("streak_center_x").setValue(this.streak_center_x);
            scope.resolve("streak_offset").setValue(this._streak_offset);
            this._streak_offset += 0.1

            pc.drawFullscreenQuad(device, outputTarget, this.vertexBuffer, this.speedShader, rect);
        }
    });

    return {
        SpeedEffect: SpeedEffect
    };
}());
