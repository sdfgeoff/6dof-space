function InfernoTextHUD(div){
    self = this
    self.area = div

    self.big_text = document.getElementById('bigtext')
    self.speed_text = document.getElementById('speedtext')

    self._remove_time = 0.0

    self.setBigText = function(value){
        self.big_text.innerHTML = value
        self._remove_time = Date.now() + 2000
    }

    self.update = function(){
        if (Date.now() > self._remove_time){
            self.big_text.innerHTML = ""
        }
    }

    self.set_speed = function(val){
        self.speed_text.innerHTML = Math.round(val.z * 100, 2) / 10
    }

    self.clear = function(val){
        self.big_text.innerHTML = ""
        self.speed_text.innerHTML = ""

    }

}
