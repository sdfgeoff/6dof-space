/* globals pc: true */
Inferno.types.PlayerExtras = function(app){
    "use strict"
    var self = this
    self.app = app
    self.hud = new Inferno.types.PlayerHud(app)


    // Things to do with loading/creating the player the extra objects
    self.load = function(loader, percent_callback, done_callback){
        var obj_load_helper = new InfernoObjectListLoader(
            app, "Player",
            ['HUD'],
            percent_callback, done_callback
        )
        obj_load_helper.start()
    }

    self.init = function(parent_entity){
        self.obj = parent_entity
        self._create_camera()
        self._create_post_effect()
        self._create_lights()
        self.hud.init(parent_entity)

    }

    self._create_camera = function(){
        self.camera = new pc.Entity();
        self.camera.addComponent("camera", {
            clearColor: new pc.Color(0.0, 0.0, 0.0),
            fov: 100,
            farClip: 40,
            nearClip: 0.01,  // Player radius is 0.25, so in theory 0.2 should
                             // be enough. But there seems to be a bug in
                             // playcanvas, so it's not...
        });
        self.camera.setPosition(0,0,0)
        self.camera.rotateLocal(90,0,0)
        self.obj.addChild(self.camera)
        self.camera.enabled = true
    }

    self._create_post_effect = function(){
        self.post_effect = new pc.SpeedEffect(app.graphicsDevice)
        self.camera.camera.postEffects.addEffect(self.post_effect);
    }

    self._create_lights = function(){
        // Create an Entity with a point light component
        self.head_light = new pc.Entity()
        self.head_light.addComponent("light", {
            type: "spot",
            color: new pc.Color(1, 1, 1),
            range: 20,
            outerConeAngle: 70,
            innerConeAngle: 50,
            intensity: 1.0,
        })
        self.head_light.rotateLocal(180,0,0)
        self.obj.addChild(self.head_light)

        self.warn_light = new pc.Entity()
        self.warn_light.addComponent("light", {
            type: "point",
            color: new pc.Color(1, 0, 0),
            range: 1.5,
            intensity: 1.0,
        })
        self.obj.addChild(self.warn_light)

    }
}
Inferno.types.PlayerHud = function(app){
    "use strict"
    var self = this
    self.app = app

    self.init = function(parent_entity){
        self.obj = new pc.Entity()
        self.obj.addComponent("model", {
            type: "asset",
            asset: self.app.assets.find('HUD.json'),
            //castShadows: true
        })
        self.obj.rotateLocal(90,0,0)
        parent_entity.addChild(self.obj)

        self.objs = {}
        for (var obj in self.obj.model.meshInstances){
            obj = self.obj.model.meshInstances[obj]
            self.objs[obj.node.name] = obj
        }
        self.add_graph_shader(self.objs['RightIndicator'])
        self.add_graph_shader(self.objs['LeftIndicatorA'])
        self.add_graph_shader(self.objs['LeftIndicatorB'])

        self._death_plane_level = 1.0
        self.setup_death_plane(self.objs['HUD'])
    }

    self.setup_death_plane = function(mesh_instance){
        var image = mesh_instance.material.diffuseMap;
        var material = new pc.Material()
        material.name = mesh_instance.node.name
        material.blendType = pc.BLEND_ADDITIVEALPHA
        material.setShader(new pc.Shader(self.app.graphicsDevice, {
            attributes: {
                aPosition: pc.SEMANTIC_POSITION,
                aUv0: pc.SEMANTIC_TEXCOORD0,
                aColor: pc.SEMANTIC_COLOR
            },
            vshader: ["attribute vec3 aPosition;",
"attribute vec2 aUv0;",
"attribute vec4 aColor;",
"",
"uniform mat4   matrix_viewProjection;",
"uniform mat4   matrix_model;",
"",
"varying vec2 UVpos;",
"varying vec4 vertexColor;",
"",
"void main(void)",
"{",
"    UVpos = aUv0.xy;",
"    vertexColor = aColor;",
"    gl_Position = matrix_viewProjection * matrix_model * vec4(aPosition, 1.0);",
"}",
""].join("\n"),
            fshader: "precision " + self.app.graphicsDevice.precision + " float;" +
                ["",
"uniform sampler2D uDiffuseMap;",
"uniform float level;",
"",
"varying vec2 UVpos;",
"varying vec4 vertexColor;",
"",
"#define MAIN_COLOR vec3(0.0, 1.0, 1.0)",
"",
"void main(void)",
"{",
"    float noise = texture2D(uDiffuseMap, UVpos).r;",
"    noise = noise * 2.0;",
"    noise += vertexColor.r;",
"    noise = noise / 2.0;",
"    noise = level*3.0 - noise;",
"    vec3 vec_noise = vec3(noise);",
"",
"    //Pegtop's Formula",
"    vec3 out_col = MAIN_COLOR * vec_noise + vec_noise;",
"",
"",
"    gl_FragColor.rgb = out_col;",
"    gl_FragColor.a = 1.0;",
"}",
""].join("\n")
        }))
        material.setParameter('uDiffuseMap', image)
        material.setParameter('level', 1.0)
        mesh_instance.material = material
    }

    self.get_death_plane_level = function(percent){
        return self._death_plane_level
    }
    self.set_death_plane_level = function(percent){
        self._death_plane_level = percent
        self.objs['HUD'].setParameter('level', percent)
    }

    self.add_graph_shader = function(mesh_instance){
        var image = mesh_instance.material.diffuseMap;
        var material = new pc.Material()
        material.name = mesh_instance.node.name
        material.blendType = pc.BLEND_ADDITIVEALPHA
        material.setShader(new pc.Shader(self.app.graphicsDevice, {
            attributes: {
                aPosition: pc.SEMANTIC_POSITION,
                aUv0: pc.SEMANTIC_TEXCOORD0,
                aColor: pc.SEMANTIC_COLOR
            },
            vshader: ["attribute vec3 aPosition;",
"attribute vec2 aUv0;",
"attribute vec4 aColor;",
"",
"uniform mat4   matrix_viewProjection;",
"uniform mat4   matrix_model;",
"",
"varying vec2 UVpos;",
"varying vec4 vertexColor;",
"",
"void main(void)",
"{",
"    UVpos = aUv0.xy;",
"    vertexColor = aColor;",
"    gl_Position = matrix_viewProjection * matrix_model * vec4(aPosition, 1.0);",
"}",
""].join("\n"),
            fshader: "precision " + self.app.graphicsDevice.precision + " float;" +
                ["",
"uniform sampler2D uDiffuseMap;",
"uniform float level;",
"",
"varying vec2 UVpos;",
"varying vec4 vertexColor;",
"",
"",
"void main(void)",
"{",
"    float graph_static = UVpos.x;",
"    if (graph_static > level){",
"        graph_static = 1.0;",
"    } else {",
"        graph_static = 0.0;",
"    }",
"    graph_static = pow(graph_static, 2.2);",
"    gl_FragColor = vertexColor * graph_static;",
"}",
""].join("\n")
        }))
        material.setParameter('uDiffuseMap', image)
        material.setParameter('level', 0.99)
        mesh_instance.material = material
    }

    self.set_shield_percent = function(bank){
        var percent = bank.percent() * 0.98 + 0.01  // Correction for texture wrapping/borders
        self.objs['RightIndicator'].setParameter('level', 1.0 - percent)
        self.objs['LowShieldWarning'].visible = (percent < 0.3)
    }

    self.set_boost_percent = function(bank){
        var percent = bank.percent() * 0.98 + 0.01
        self.objs['LeftIndicatorA'].setParameter('level', 1.0 - percent)
    }

    self.set_slow_percent = function(bank){
        var percent = bank.percent() * 0.98 + 0.01
        self.objs['LeftIndicatorB'].setParameter('level', 1.0 - percent)
    }

    self.show_velocity = function(vec){
        self.objs['DirectionIndicator'].node.setLocalPosition(vec.x/300.0, -vec.y/300.0, -0.05)
    }
}


